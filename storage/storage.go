package storage

import (
	"context"

	"last-exam/online_market_go_product_service/genproto/product_service"
	"last-exam/online_market_go_product_service/models"
)

type StorageI interface {
	CloseDB()
	Product() ProductRepoI
	Category() CategoryRepoI
}

type ProductRepoI interface {
	CreateProductFunction(ctx context.Context, req *product_service.CreateProduct) (resp *product_service.ProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.Product, err error)
	GetAll(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error)
	GetUserProductsFunction(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error)
	UpdateProductFunction(ctx context.Context, req *product_service.UpdateProduct) (rowsAffected int64, err error)
	UpdatePatchProductFunction(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	ReduseCountOfProduct(ctx context.Context, req *product_service.ProductTransaction) error
	DeleteProduct(ctx context.Context, req *product_service.ProductPrimaryKey) error
}

type CategoryRepoI interface {
	Create(ctx context.Context, req *product_service.CreateCategory) (resp *product_service.CategoryPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *product_service.CategoryPrimaryKey) (resp *product_service.Category, err error)
	GetAll(ctx context.Context, req *product_service.GetListCategoryRequest) (resp *product_service.GetListCategoryResponse, err error)
	Update(ctx context.Context, req *product_service.UpdateCategory) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *product_service.CategoryPrimaryKey) error
}
