package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"last-exam/online_market_go_product_service/genproto/product_service"
	"last-exam/online_market_go_product_service/models"
	"last-exam/online_market_go_product_service/pkg/helper"
	"last-exam/online_market_go_product_service/storage"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) CreateProductFunction(ctx context.Context, req *product_service.CreateProduct) (resp *product_service.ProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "product" (
				id,
				name,
				owner_id,
				status,
				price,
				product_amount,
				category_id,
				updated_at
			) VALUES ( $1, $2, $3, $4, $5, $6, $7, now() )
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
		req.OwnerId,
		req.Status,
		req.Price,
		req.ProductAmount,
		req.CategoryId,
	)

	if err != nil {
		return nil, err
	}

	return &product_service.ProductPrimaryKey{Id: id.String(), UserId: req.OwnerId, MessageStatus: "seller"}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.Product, err error) {

	var query string

	var (
		id             sql.NullString
		name           sql.NullString
		owner_id       sql.NullString
		status         sql.NullString
		price          sql.NullFloat64
		product_amount sql.NullInt32
		cId            sql.NullString
		categoryName   sql.NullString
		parentId       sql.NullString
		createdAt      sql.NullString
		updatedAt      sql.NullString
	)

	if req.MessageStatus == "customer" {
		query = `
			SELECT
				p.id,
				p.name,
				p.owner_id,
				p.status,
				p.price,
				p.product_amount,
				c.id,
				c.name,
				c.parent_id,
				p.created_at,
				p.updated_at
			FROM "product" as p
			JOIN "category" as c ON c.id = p.category_id 
			WHERE deleted_at IS NULL AND id = $1 AND product_amount > 0
		`

		err = c.db.QueryRow(ctx, query, req.Id).Scan(
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentId,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			return resp, err
		}
	} else if req.MessageStatus == "seller" {
		query = `
			SELECT
				p.id,
				p.name,
				p.owner_id,
				p.status,
				p.price,
				p.product_amount,
				c.id,
				c.name,
				c.parent_id,
				p.created_at,
				p.updated_at
			FROM "product" as p
			JOIN "category" as c ON c.id = p.category_id 
			WHERE (p.deleted_at IS NULL AND p.id = $1 AND p.owner_id = $2) OR (p.deleted_at IS NULL AND p.id = $1 AND p.product_amount > 0)
		`

		err = c.db.QueryRow(ctx, query, req.Id, req.UserId).Scan(
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentId,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			return resp, err
		}
	} else if req.MessageStatus == "order" {
		query = `
			SELECT
				p.id,
				p.name,
				p.owner_id,
				p.status,
				p.price,
				p.product_amount,
				c.id,
				c.name,
				c.parent_id,
				p.created_at,
				p.updated_at
			FROM "product" as p
			JOIN "category" as c ON c.id = p.category_id 
			WHERE p.id = $1
		`

		err = c.db.QueryRow(ctx, query, req.Id).Scan(
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentId,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			return resp, err
		}
	}

	resp = &product_service.Product{
		Id:            id.String,
		Name:          name.String,
		OwnerId:       owner_id.String,
		Status:        status.String,
		Price:         price.Float64,
		ProductAmount: product_amount.Int32,
		CreatedAt:     createdAt.String,
		UpdatedAt:     updatedAt.String,
	}

	resp.ProductCategories = &product_service.CategoryLists{
		Id:         cId.String,
		Name:       categoryName.String,
		ParentName: parentId.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error) {

	resp = &product_service.GetListProductResponse{}

	var (
		queryTop      string
		queryTopOwner string
		queryCustom   string
		limit         = ""
		offset        = " OFFSET 0 "
		params        = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY p.created_at DESC"
	)

	resp.CountOrdinary = 5

	if req.MessageStatus == "seller" {
		queryTopOwner = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'top'
		`
		queryTopOwner += "AND p.owner_id = '" + fmt.Sprintf(req.OwnerId) + "'"

		rows, err := c.db.Query(ctx, queryTopOwner)
		defer rows.Close()

		if err != nil {
			return resp, err
		}

		for rows.Next() {
			var (
				id             sql.NullString
				name           sql.NullString
				owner_id       sql.NullString
				status         sql.NullString
				price          sql.NullFloat64
				product_amount sql.NullInt32
				cId            sql.NullString
				categoryName   sql.NullString
				parentName     sql.NullString
				createdAt      sql.NullString
				updatedAt      sql.NullString
			)

			err := rows.Scan(
				&resp.Count,
				&id,
				&name,
				&owner_id,
				&status,
				&price,
				&product_amount,
				&cId,
				&categoryName,
				&parentName,
				&createdAt,
				&updatedAt,
			)

			if err != nil {
				return resp, err
			}

			ex := &product_service.CategoryLists{
				Id:         cId.String,
				Name:       categoryName.String,
				ParentName: parentName.String,
			}

			resp.ProductsTop = append(resp.ProductsTop, &product_service.Product{
				Id:                id.String,
				Name:              name.String,
				OwnerId:           owner_id.String,
				Status:            status.String,
				Price:             price.Float64,
				ProductAmount:     product_amount.Int32,
				ProductCategories: ex,
				CreatedAt:         createdAt.String,
				UpdatedAt:         updatedAt.String,
			})
		}

		old_count := resp.Count

		queryTop = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'top'
		`
		queryTop += "AND p.owner_id <> '" + fmt.Sprintf(req.OwnerId) + "'"

		if req.GetLimit() > 0 {
			limit = " LIMIT :limit"
			params["limit"] = req.Limit
		}

		if req.GetOffset() > 0 {
			offset = " OFFSET :offset"
			params["offset"] = req.Offset
		}

		queryTop += sort + offset + limit

		queryTop, args := helper.ReplaceQueryParams(queryTop, params)
		rows, err = c.db.Query(ctx, queryTop, args...)
		defer rows.Close()

		if err != nil {
			return resp, err
		}

		for rows.Next() {
			var (
				id             sql.NullString
				name           sql.NullString
				owner_id       sql.NullString
				status         sql.NullString
				price          sql.NullFloat64
				product_amount sql.NullInt32
				Cid            sql.NullString
				categoryName   sql.NullString
				parentName     sql.NullString
				createdAt      sql.NullString
				updatedAt      sql.NullString
			)

			err := rows.Scan(
				&resp.Count,
				&id,
				&name,
				&owner_id,
				&status,
				&price,
				&product_amount,
				&Cid,
				&categoryName,
				&parentName,
				&createdAt,
				&updatedAt,
			)

			if err != nil {
				return resp, err
			}

			ex := &product_service.CategoryLists{
				Id:         Cid.String,
				Name:       categoryName.String,
				ParentName: parentName.String,
			}

			resp.ProductsTop = append(resp.ProductsTop, &product_service.Product{
				Id:                id.String,
				Name:              name.String,
				OwnerId:           owner_id.String,
				Status:            status.String,
				Price:             price.Float64,
				ProductAmount:     product_amount.Int32,
				ProductCategories: ex,
				CreatedAt:         createdAt.String,
				UpdatedAt:         updatedAt.String,
			})

		}

		resp.Count += old_count - 1

		queryCustom = `
		SELECT
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id	
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'custom'
		ORDER BY p.created_at DESC
		OFFSET 0
		LIMIT 5
		`

		rows, err = c.db.Query(ctx, queryCustom)
		defer rows.Close()

		if err != nil {
			return resp, err
		}

		for rows.Next() {
			var (
				id             sql.NullString
				name           sql.NullString
				owner_id       sql.NullString
				status         sql.NullString
				price          sql.NullFloat64
				product_amount sql.NullInt32
				cId            sql.NullString
				categoryName   sql.NullString
				parentName     sql.NullString
				createdAt      sql.NullString
				updatedAt      sql.NullString
			)

			err := rows.Scan(
				&id,
				&name,
				&owner_id,
				&status,
				&price,
				&product_amount,
				&cId,
				&categoryName,
				&parentName,
				&createdAt,
				&updatedAt,
			)

			if err != nil {
				return resp, err
			}

			ex := &product_service.CategoryLists{}

			resp.ProductsOrdinary = append(resp.ProductsOrdinary, &product_service.Product{
				Id:                id.String,
				Name:              name.String,
				OwnerId:           owner_id.String,
				Status:            status.String,
				Price:             price.Float64,
				ProductAmount:     product_amount.Int32,
				ProductCategories: ex,
				CreatedAt:         createdAt.String,
				UpdatedAt:         updatedAt.String,
			})
		}

		return resp, nil
	}

	queryTop = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'top'
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	queryTop += sort + offset + limit

	queryTop, args := helper.ReplaceQueryParams(queryTop, params)
	rows, err := c.db.Query(ctx, queryTop, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			name           sql.NullString
			owner_id       sql.NullString
			status         sql.NullString
			price          sql.NullFloat64
			product_amount sql.NullInt32
			cId            sql.NullString
			categoryName   sql.NullString
			parentName     sql.NullString
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentName,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		ex := &product_service.CategoryLists{
			Id:         cId.String,
			Name:       categoryName.String,
			ParentName: parentName.String,
		}

		resp.ProductsTop = append(resp.ProductsTop, &product_service.Product{
			Id:                id.String,
			Name:              name.String,
			OwnerId:           owner_id.String,
			Status:            status.String,
			Price:             price.Float64,
			ProductAmount:     product_amount.Int32,
			ProductCategories: ex,
			CreatedAt:         createdAt.String,
			UpdatedAt:         updatedAt.String,
		})
	}

	queryCustom = `
		SELECT
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'custom'
		ORDER BY p.created_at DESC
		OFFSET 0
		LIMIT 5
	`

	rows, err = c.db.Query(ctx, queryCustom)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			name           sql.NullString
			owner_id       sql.NullString
			status         sql.NullString
			price          sql.NullFloat64
			product_amount sql.NullInt32
			cId            sql.NullString
			categoryName   sql.NullString
			parentName     sql.NullString
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentName,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		ex := &product_service.CategoryLists{
			Id:         cId.String,
			Name:       categoryName.String,
			ParentName: parentName.String,
		}

		resp.ProductsOrdinary = append(resp.ProductsOrdinary, &product_service.Product{
			Id:                id.String,
			Name:              name.String,
			OwnerId:           owner_id.String,
			Status:            status.String,
			Price:             price.Float64,
			ProductAmount:     product_amount.Int32,
			ProductCategories: ex,
			CreatedAt:         createdAt.String,
			UpdatedAt:         updatedAt.String,
		})
	}

	return resp, nil
}

func (c *ProductRepo) GetUserProductsFunction(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error) {

	resp = &product_service.GetListProductResponse{}

	var (
		queryTop    string
		queryCustom string
		limit       = ""
		offset      = " OFFSET 0 "
		params      = make(map[string]interface{})
		// filter = " WHERE TRUE"
		sort = " ORDER BY p.created_at DESC"
	)

	queryTop = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.name,
			p.owner_id,
			p.status,
			p.price,
			p.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'top'
	`

	queryTop += "AND p.owner_id = '" + fmt.Sprintf(req.OwnerId) + "'"

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	queryTop += sort + offset + limit

	queryTop, args := helper.ReplaceQueryParams(queryTop, params)
	rows, err := c.db.Query(ctx, queryTop, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			name           sql.NullString
			owner_id       sql.NullString
			status         sql.NullString
			price          sql.NullFloat64
			product_amount sql.NullInt32
			cId            sql.NullString
			categoryName   sql.NullString
			parentName     sql.NullString
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentName,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		ex := &product_service.CategoryLists{
			Id:         cId.String,
			Name:       categoryName.String,
			ParentName: parentName.String,
		}

		resp.ProductsTop = append(resp.ProductsTop, &product_service.Product{
			Id:                id.String,
			Name:              name.String,
			OwnerId:           owner_id.String,
			Status:            status.String,
			Price:             price.Float64,
			ProductAmount:     product_amount.Int32,
			ProductCategories: ex,
			CreatedAt:         createdAt.String,
			UpdatedAt:         updatedAt.String,
		})
	}

	queryCustom = `
		SELECT
			COUNT(*) OVER(),
			p.id,
			p.name,
			p.owner_id,
			p.status,
			P.price,
			P.product_amount,
			c.id,
			c.name,
			c.parent_id,
			TO_CHAR(p.created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(p.updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "product" as p
		JOIN "category" as c ON c.id = p.category_id
		WHERE p.deleted_at IS NULL AND p.product_amount > 0 AND p.status = 'custom'
	`
	queryCustom += "AND p.owner_id = '" + fmt.Sprintf(req.OwnerId) + "'"

	queryCustom += sort + offset + limit

	queryCustom, args = helper.ReplaceQueryParams(queryCustom, params)
	rows, err = c.db.Query(ctx, queryCustom, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			name           sql.NullString
			owner_id       sql.NullString
			status         sql.NullString
			price          sql.NullFloat64
			product_amount sql.NullInt32
			cId            sql.NullString
			categoryName   sql.NullString
			parentName     sql.NullString
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.CountOrdinary,
			&id,
			&name,
			&owner_id,
			&status,
			&price,
			&product_amount,
			&cId,
			&categoryName,
			&parentName,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		ex := &product_service.CategoryLists{
			Id:         cId.String,
			Name:       categoryName.String,
			ParentName: parentName.String,
		}

		resp.ProductsOrdinary = append(resp.ProductsOrdinary, &product_service.Product{
			Id:                id.String,
			Name:              name.String,
			OwnerId:           owner_id.String,
			Status:            status.String,
			Price:             price.Float64,
			ProductAmount:     product_amount.Int32,
			ProductCategories: ex,
			CreatedAt:         createdAt.String,
			UpdatedAt:         updatedAt.String,
		})
	}

	return resp, nil
}

func (c *ProductRepo) UpdateProductFunction(ctx context.Context, req *product_service.UpdateProduct) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "product"
			SET
				name = :name,
				owner_id = :owner_id,
				status = :status,
				price,
				product_amount,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":             req.GetId(),
		"name":           req.GetName(),
		"owner_id":       req.GetOwnerId(),
		"status":         req.GetStatus(),
		"price":          req.GetPrice(),
		"product_amount": req.GetProductAmount(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) UpdatePatchProductFunction(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"product"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *ProductRepo) ReduseCountOfProduct(ctx context.Context, req *product_service.ProductTransaction) error {

	query := `UPDATE "product" SET product_amount = $2 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.ProductId, req.OrderedAmount)

	if err != nil {
		return err
	}

	return nil
}

func (c *ProductRepo) DeleteProduct(ctx context.Context, req *product_service.ProductPrimaryKey) error {

	query := `UPDATE "product" SET deleted_at = now() WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
