package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"last-exam/online_market_go_product_service/config"
	"last-exam/online_market_go_product_service/genproto/product_service"
	"last-exam/online_market_go_product_service/grpc/client"
	"last-exam/online_market_go_product_service/grpc/service"
	"last-exam/online_market_go_product_service/pkg/logger"
	"last-exam/online_market_go_product_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	product_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	product_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
