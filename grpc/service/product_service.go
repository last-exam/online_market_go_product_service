package service

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"last-exam/online_market_go_product_service/config"
	"last-exam/online_market_go_product_service/genproto/product_service"
	"last-exam/online_market_go_product_service/genproto/user_service"
	"last-exam/online_market_go_product_service/grpc/client"
	"last-exam/online_market_go_product_service/models"
	"last-exam/online_market_go_product_service/pkg/logger"
	"last-exam/online_market_go_product_service/storage"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*product_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ProductService) CreateProductFunc(ctx context.Context, req *product_service.CreateProduct) (resp *product_service.Product, err error) {

	i.log.Info("---CreateProduct------>", logger.Any("req", req))

	if req.Status == "top" {
		_, err = i.services.UserService().UserMoneyExchangeWithdraw(ctx, &user_service.UserTransaction{UserId: req.OwnerId, AmountOfMoney: float64(7000)})
		if err != nil {
			i.log.Error("!!!UserMoneyExchangeWithdraw->user->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else if req.Status == "custom" {
		_, err = i.services.UserService().UserMoneyExchangeWithdraw(ctx, &user_service.UserTransaction{UserId: req.OwnerId, AmountOfMoney: float64(3000)})
		if err != nil {
			i.log.Error("!!!UserMoneyExchangeWithdraw->user->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		i.log.Error("!!!UserMoneyExchangeWithdraw->user->Get---> Invalid Product_Status")
		return nil, status.Error(codes.InvalidArgument, errors.New("Invalid Product_Status").Error())
	}

	pKey, err := i.strg.Product().CreateProductFunction(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateProduct->Product->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Product().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ProductService) GetByID(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *product_service.Product, err error) {

	i.log.Info("---GetProductByID------>", logger.Any("req", req))

	resp, err = i.strg.Product().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetProductByID->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// fmt.Println("hello", resp)

	user, err := i.services.UserService().GetByID(ctx, &user_service.UserPrimaryKey{Id: resp.OwnerId})
	if err != nil {
		i.log.Error("!!!GetUserByID->user->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp.Owner = &product_service.ProductOwner{
		OwnerId:    user.Id,
		OwnerName:  user.FirstName + " " + user.LastName,
		OwnerPhone: user.PhoneNumber,
		CreatedAt:  user.CreatedAt,
		UpdatedAt:  user.UpdatedAt,
	}

	return
}

func (i *ProductService) GetList(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error) {

	i.log.Info("---GetProducts------>", logger.Any("req", req))

	if req.MessageStatus == "customer" || req.MessageStatus == "seller" {
		resp, err = i.strg.Product().GetAll(ctx, req)
		if err != nil {
			i.log.Error("!!!GetProducts->Product->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		var arr []*product_service.Product

		for k := 0; k < len(resp.ProductsTop); k++ {
			if resp.ProductsTop[k].OwnerId != req.OwnerId {
				arr = append(arr, resp.ProductsTop[k])
			}
		}

		rand.Seed(time.Now().UnixNano())
		rand.Shuffle(len(arr), func(i, j int) {
			arr[i], arr[j] = arr[j], arr[i]
		})

		for k := 0; k < len(arr); k++ {
			if resp.ProductsTop[k].OwnerId != req.OwnerId {
				resp.ProductsTop[k] = arr[k]
			}
		}

	} else {
		i.log.Error("!!!GetProducts->Product->Get---> Invalid user status")
		return nil, status.Error(codes.InvalidArgument, errors.New("Invalid user status").Error())
	}

	return
}

func (i *ProductService) GetUserProducts(ctx context.Context, req *product_service.GetListProductRequest) (resp *product_service.GetListProductResponse, err error) {
	i.log.Info("---GetUserProducts------>", logger.Any("req", req))

	if req.MessageStatus == "seller" {
		resp, err = i.strg.Product().GetUserProductsFunction(ctx, req)
		if err != nil {
			i.log.Error("!!!GetUserProducts->Product->Get--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		for j := 0; j < int(resp.Count); j++ {

			user, err := i.services.UserService().GetByID(ctx, &user_service.UserPrimaryKey{Id: resp.ProductsTop[j].OwnerId})
			if err != nil {
				i.log.Error("!!!GetUserByID->user->Get--->", logger.Error(err))
				return nil, status.Error(codes.InvalidArgument, err.Error())
			}

			resp.ProductsTop[j].Owner = &product_service.ProductOwner{
				OwnerId:    user.Id,
				OwnerName:  user.FirstName + " " + user.LastName,
				OwnerPhone: user.PhoneNumber,
				CreatedAt:  user.CreatedAt,
				UpdatedAt:  user.UpdatedAt,
			}
		}

		for j := 0; j < int(resp.CountOrdinary); j++ {

			user, err := i.services.UserService().GetByID(ctx, &user_service.UserPrimaryKey{Id: resp.ProductsOrdinary[j].OwnerId})
			if err != nil {
				i.log.Error("!!!GetUserByID->user->Get--->", logger.Error(err))
				return nil, status.Error(codes.InvalidArgument, err.Error())
			}

			resp.ProductsOrdinary[j].Owner = &product_service.ProductOwner{
				OwnerId:    user.Id,
				OwnerName:  user.FirstName + " " + user.LastName,
				OwnerPhone: user.PhoneNumber,
				CreatedAt:  user.CreatedAt,
				UpdatedAt:  user.UpdatedAt,
			}
		}
	} else {
		i.log.Error("!!!GetUserProducts->Product->Get---> Invalid user status")
		return nil, status.Error(codes.InvalidArgument, errors.New("Invalid user status").Error())
	}

	return
}

func (i *ProductService) UpdateProductFunc(ctx context.Context, req *product_service.UpdateProduct) (resp *product_service.Product, err error) {

	i.log.Info("---UpdateProduct------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Product().UpdateProductFunction(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &product_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) UpdatePatchProductFunc(ctx context.Context, req *product_service.UpdatePatchProduct) (resp *product_service.Product, err error) {

	i.log.Info("---UpdatePatchProduct------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Product().UpdatePatchProductFunction(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Product().GetByPKey(ctx, &product_service.ProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ProductService) ProductMoneyExchange(ctx context.Context, req *product_service.ProductTransaction) (resp *product_service.ProductPrimaryKey, err error) {

	fmt.Println(":::12", req)

	product, err := i.strg.Product().GetByPKey(ctx, &product_service.ProductPrimaryKey{Id: req.ProductId, MessageStatus: "order"})
	if err != nil {
		i.log.Error("!!!GetProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	fmt.Println(":::1", product)

	count := product.ProductAmount - req.OrderedAmount

	err = i.strg.Product().ReduseCountOfProduct(ctx, &product_service.ProductTransaction{ProductId: req.ProductId, OrderedAmount: count})
	if err != nil {
		i.log.Error("!!!ReduseCountOfProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	fmt.Println(":;:", product.OwnerId, "::", req.AmountOfMoney)

	_, err = i.services.UserService().UserMoneyExchangeDeposit(ctx, &user_service.UserTransaction{UserId: product.OwnerId, AmountOfMoney: req.AmountOfMoney})
	if err != nil {
		i.log.Error("!!!UserMoneyExchangeDeposit->user->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp = &product_service.ProductPrimaryKey{Id: req.ProductId}

	return resp, nil
}

func (i *ProductService) DeleteProduct(ctx context.Context, req *product_service.ProductPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteProduct------>", logger.Any("req", req))

	err = i.strg.Product().DeleteProduct(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteProduct->Product->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
