CREATE TABLE IF NOT EXISTS "category"(
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL UNIQUE,
    parent_id UUID REFERENCES category(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "product" (
    id UUID PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    owner_id UUID NOT NULL,
    status VARCHAR(6) NOT NULL,
    price NUMERIC NOT NULL,
    product_amount INTEGER NOT NULL,
    category_id UUID not NULL REFERENCES "category" (id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);